import { hashString } from "../util/hash";
/**
 * This function emulates a function responsible for calling the backend service
 * which is responsible for registering a new user
 */
export function signUp(user) {
  let { userName, email, password } = user;
  let hashedPassword = hashString(password);
  let userDataToStore = {
    userName,
    email,
    hashedPassword,
  };
  let usersListAsAnObject = JSON.parse(localStorage.getItem("users")) || {};
  if (usersListAsAnObject[userName]) {
    let exception = {
      name: "alreadyExists",
      message: "A User Already Registered With The Same User Name",
    };
    throw exception;
  }
  usersListAsAnObject[userName] = userDataToStore;
  localStorage.setItem("users", JSON.stringify(usersListAsAnObject));
  localStorage.setItem(
    "currentUser",
    JSON.stringify(usersListAsAnObject[userName])
  );
}

/**
 * This function emulates a function responsible for calling the backend service
 * which is responsible for authenticating a user
 */
export function logIn(user) {
  let { userName, password } = user;
  let hashedPassword = hashString(password);
  let usersListAsAnObject = JSON.parse(localStorage.getItem("users")) || {};
  if (usersListAsAnObject[userName]) {
    let foundUser = usersListAsAnObject[userName];
    if (foundUser.hashedPassword == hashedPassword) {
      localStorage.setItem("currentUser", JSON.stringify(foundUser));
      return { status: 200, data: foundUser };
    } else {
      let exception = {
        name: "unauthenticated",
        message: "Unauthenticated: wrong user name or password",
      };
      throw exception;
    }
  } else {
    let exception = {
      name: "unauthenticated",
      message: "Unauthenticated: wrong user name or password",
    };
    throw exception;
  }
}
/**
 * This function is responsible for logingout the current loggedin user
 */
export function logOut() {
  localStorage.removeItem("currentUser");
}

/**
 * This function is responsible for getting the current loggedin user
 */
export function getCurrentLoggedInUser() {
  try {
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return currentUser;
  } catch (exception) {
    return null;
  }
}

export default { signUp, logIn, logOut, getCurrentLoggedInUser };
