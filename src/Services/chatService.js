import _ from "lodash";
import AuthService from "./authService";

/**
 * This function is responsible for getting the contact list of the
 *  loggedin user. * This contact list represents all the system users
 * This list represented by an array
 * and each element of it has the following properties:
 * name, lastMessage, unreadMessagesCount
 */
export function getCurrentUserContactsList() {
  let currentUser = AuthService.getCurrentLoggedInUser();
  let usersListAsAnObject = JSON.parse(localStorage.getItem("users")) || {};
  delete usersListAsAnObject[currentUser["userName"]];
  let contacts = [];
  for (let user in usersListAsAnObject) {
    let contact = {};
    contact.name = usersListAsAnObject[user].userName;
    contact.lastMessage = getLastMessageBetween(
      currentUser.userName,
      usersListAsAnObject[user].userName
    ).text;
    contact.unreadMessagesCount = getCurrentUserUnreadMessagesCount(
      usersListAsAnObject[user].userName
    );
    contacts.push(contact);
  }

  return contacts;
}

/**
 * This function is responsible for
 * sending a message from the loggedin user to another user.
 * @param toUserName the user name of the targeted user
 * @param messageText the message text
 */
export function sendMessageFromTheCurrentUser(toUserName, messageText) {
  let currentUser = AuthService.getCurrentLoggedInUser();
  let currentUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(currentUser.userName + "SentMessages")) ||
    {};
  let message = {};
  message.from = currentUser.userName;
  message.to = toUserName;
  message.date = Date.now();
  message.text = messageText;
  message.unread = true;
  currentUserMessageListAsAnObject[JSON.stringify(Date.now())] = message;
  localStorage.setItem(
    currentUser.userName + "SentMessages",
    JSON.stringify(currentUserMessageListAsAnObject)
  );
}

/**
 * This function is responsible for geting
 * the conversation of the loggedin user with other user.
 * The returned conversation is an array of conversation elements
 * and each conversation element is an
 * object that has the following properties:
 * 1-from:sender user name
 * 2-sentMessage: empty string if the sender is not the loggedin user
 * 3-recievedMessage: empty string if the sender is not the other user
 * 4-date: the date object of message sending time.
 * 5-unread: status of message
 * @param otherUserName the user name of the other user
 */
export function getCurrentUserCoversationWithOtherUser(otherUserName) {
  let currentUser = AuthService.getCurrentLoggedInUser();
  let fromCurrentUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(currentUser.userName + "SentMessages")) ||
    {};
  let fromUserMessageListAsAnArray = [];
  for (let message in fromCurrentUserMessageListAsAnObject) {
    if (fromCurrentUserMessageListAsAnObject[message].to == otherUserName) {
      fromUserMessageListAsAnArray.push(
        fromCurrentUserMessageListAsAnObject[message]
      );
    }
  }
  let fromOtherUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(otherUserName + "SentMessages")) || {};
  let fromOtherUserMessageListAsAnArray = [];
  for (let message in fromOtherUserMessageListAsAnObject) {
    if (
      fromOtherUserMessageListAsAnObject[message].to == currentUser.userName
    ) {
      fromOtherUserMessageListAsAnArray.push(
        fromOtherUserMessageListAsAnObject[message]
      );
    }
  }
  let conversationMessages = [
    ...fromUserMessageListAsAnArray,
    ...fromOtherUserMessageListAsAnArray,
  ];
  let converation = conversationMessages.map((message) => {
    return {
      from: message.from,
      sentMessage: message.from == currentUser.userName ? message.text : "",
      recivedMessage: message.to == currentUser.userName ? message.text : "",
      date: message.date,
      unread: message.unread,
    };
  });
  converation = _.orderBy(converation, ["date"], ["asc"]);
  return converation;
}

export function deleteAMessageOfTheCurrentUser(otherUerName, date) {
  let currentUserName = AuthService.getCurrentLoggedInUser().userName;
  let fromOtherUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(otherUerName + "SentMessages")) || {};
  for (let message in fromOtherUserMessageListAsAnObject) {
    if (
      fromOtherUserMessageListAsAnObject[message].to == currentUserName &&
      fromOtherUserMessageListAsAnObject[message].date == date
    ) {
      delete fromOtherUserMessageListAsAnObject[message];
      localStorage.setItem(
        otherUerName + "SentMessages",
        JSON.stringify(fromOtherUserMessageListAsAnObject)
      );
    }
  }
  let fromCurrentUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(currentUserName + "SentMessages")) || {};
  for (let message in fromCurrentUserMessageListAsAnObject) {
    if (
      fromCurrentUserMessageListAsAnObject[message].to == otherUerName &&
      fromCurrentUserMessageListAsAnObject[message].date == date
    ) {
      delete fromCurrentUserMessageListAsAnObject[message];
      localStorage.setItem(
        currentUserName + "SentMessages",
        JSON.stringify(fromCurrentUserMessageListAsAnObject)
      );
    }
  }
}
/**
 * This function is responsible for
 *  marking the sent messages by anothe user to the loggedin user
 * as messages that has been read by the current user
 * @param fromUserName the user name of the messages sender
 */
export function readMessagesByCurrentUser(fromUserName) {
  let currentUserName = AuthService.getCurrentLoggedInUser().userName;
  let fromOtherUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(fromUserName + "SentMessages")) || {};
  for (let message in fromOtherUserMessageListAsAnObject) {
    if (fromOtherUserMessageListAsAnObject[message].to == currentUserName) {
      fromOtherUserMessageListAsAnObject[message].unread = false;
    }
  }
  localStorage.setItem(
    fromUserName + "SentMessages",
    JSON.stringify(fromOtherUserMessageListAsAnObject)
  );
}

/**
 * This function is responsible calculating the number of the messages that
 * have been sent by some other user to the current logged in user
 * @param fromUserName the user name of the messages sender
 */

function getCurrentUserUnreadMessagesCount(fromUserName) {
  let currentUserName = AuthService.getCurrentLoggedInUser().userName;
  let fromOtherUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(fromUserName + "SentMessages")) || {};
  let unreadMessagesCount = 0;
  for (let message in fromOtherUserMessageListAsAnObject) {
    if (
      fromOtherUserMessageListAsAnObject[message].to == currentUserName &&
      fromOtherUserMessageListAsAnObject[message].unread == true
    ) {
      unreadMessagesCount++;
    }
  }
  return unreadMessagesCount;
}

/**
 * This function is responsible for getting the last message
 *  from a user to another user
 * @param fromUserName the user name of the messages sender
 * @param toUserName the user name of the messages targeted user
 */
function getLastMessage(fromUserName, toUserName) {
  let fromUserMessageListAsAnObject =
    JSON.parse(localStorage.getItem(fromUserName + "SentMessages")) || {};
  let lastMessage = {};
  lastMessage.date = 0;
  for (let message in fromUserMessageListAsAnObject) {
    if (fromUserMessageListAsAnObject[message].to == toUserName) {
      if (fromUserMessageListAsAnObject[message].date > lastMessage.date) {
        lastMessage = fromUserMessageListAsAnObject[message];
      }
    }
  }
  return lastMessage;
}

/**
 * This function is responsible for getting the last message
 *  between two users
 * @param firstUser the user name of the first user
 * @param seconUser the user name of the second user
 */
function getLastMessageBetween(firstUser, seconUser) {
  let lastMessageFromFirstUserToSeconUser = getLastMessage(
    firstUser,
    seconUser
  );
  let lastMessageFromSecondUserToFirstUser = getLastMessage(
    seconUser,
    firstUser
  );
  if (
    lastMessageFromFirstUserToSeconUser.date >=
    lastMessageFromSecondUserToFirstUser.date
  ) {
    return lastMessageFromFirstUserToSeconUser;
  } else {
    return lastMessageFromSecondUserToFirstUser;
  }
}

export default {
  getCurrentUserContactsList,
  sendMessageFromTheCurrentUser,
  getCurrentUserCoversationWithOtherUser,
  deleteAMessageOfTheCurrentUser,
  readMessagesByCurrentUser,
};
