import React, { Component } from "react";
import AuthService from "../../Services/authService";
class LogOut extends Component {
  componentDidMount() {
    AuthService.logOut();
    window.location = "/";
  }
  render() {
    return null;
  }
}

export default LogOut;
