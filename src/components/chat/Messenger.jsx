import React, { Component } from "react";
import Conversation from "./Conversation";
import _ from "lodash";
import ChatService from "./../../Services/chatService";
import ContactsList from "./ContactsList";
import SendMessageForm from "./SendMessageForm";
import NoContactSelected from "./NoContactSelected";
import { toast } from "react-toastify";
class Messenger extends Component {
  state = {
    contacts: [],
    selectedContact: null,
    selectedContactConversation: [],
  };

  componentDidMount() {
    let { contacts } = { ...this.state };
    try {
      contacts = ChatService.getCurrentUserContactsList();
    } catch (exception) {
      toast.error("can't get contacts");
    }
    this.setState({ contacts });
  }

  handleDeleteMessage = (date) => {
    let { selectedContact, selectedContactConversation, contacts } = {
      ...this.state,
    };
    try {
      ChatService.deleteAMessageOfTheCurrentUser(selectedContact.name, date);
      toast.error("message deleted");
    } catch (exception) {
      toast.error("can't delete message");
    }
    try {
      selectedContactConversation = ChatService.getCurrentUserCoversationWithOtherUser(
        selectedContact.name
      );
    } catch (exception) {
      toast.error("can't get conversation");
    }
    try {
      contacts = ChatService.getCurrentUserContactsList();
    } catch (exception) {
      toast.error("can't get contacts");
    }
    this.setState({ selectedContactConversation, contacts });
  };

  handleSelectedCotactChange = (newSelectedContact) => {
    let { selectedContactConversation, contacts } = { ...this.state };
    try {
      selectedContactConversation = [
        ...ChatService.getCurrentUserCoversationWithOtherUser(
          newSelectedContact.name
        ),
      ];
      ChatService.readMessagesByCurrentUser(newSelectedContact.name);
    } catch (exception) {
      toast.error("can't handle unread messages");
    }
    try {
      contacts = ChatService.getCurrentUserContactsList();
    } catch (exception) {
      toast.error("can't get contacts");
    }
    this.setState({
      selectedContact: newSelectedContact,
      selectedContactConversation,
      contacts,
    });
  };

  handleSendMessageFormSubmist = (message) => {
    let { selectedContact, selectedContactConversation, contacts } = {
      ...this.state,
    };
    try {
      ChatService.sendMessageFromTheCurrentUser(selectedContact.name, message);
    } catch (exception) {
      toast.error("can't send message");
    }
    try {
      selectedContactConversation = [
        ...ChatService.getCurrentUserCoversationWithOtherUser(
          selectedContact.name
        ),
      ];
    } catch (exception) {
      toast.error("can't get conversation");
    }
    try {
      contacts = ChatService.getCurrentUserContactsList();
    } catch (exception) {
      toast.error("can't get contacts");
    }
    this.setState({ selectedContactConversation, contacts });
  };

  render() {
    const {
      contacts,
      selectedContact,
      selectedContactConversation,
    } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-3">
            <ContactsList
              contacts={contacts}
              selectedContact={selectedContact}
              onSelectedContactChange={this.handleSelectedCotactChange}
            />
          </div>
          <div className="col-9">
            <div>
              {selectedContact ? (
                <div>
                  <h2> {selectedContact ? selectedContact.name : ""}</h2>
                  <div className="conversation-div">
                    <Conversation
                      conversation={selectedContactConversation}
                      onDelete={this.handleDeleteMessage}
                    />
                  </div>
                  <SendMessageForm
                    onSendMessageFormSubmist={this.handleSendMessageFormSubmist}
                  />
                </div>
              ) : (
                <NoContactSelected />
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Messenger;
