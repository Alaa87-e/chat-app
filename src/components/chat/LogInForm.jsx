import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/Form";
import AuthService from "../../Services/authService";

class LogInForm extends Form {
  state = {
    data: {
      userName: "",
      password: "",
    },
    errors: {},
  };
  schema = {
    userName: Joi.string().required().label("Username"),
    password: Joi.string().required().label("Password"),
  };

  doSubmit = () => {
    let { errors, data } = { ...this.state };
    try {
      AuthService.logIn(data);
      window.location = "/";
    } catch (exception) {
      errors = errors || {};
      errors["submitingResuls"] = exception.message;
      this.setState({ errors, data });
    }
  };

  render() {
    return (
      <div>
        <h1>Log in</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("userName", "User Name")}
          {this.renderInput("password", "Password", "password")}
          {this.renderValidationSummary()}
          {this.renderButton("Login")}
        </form>
      </div>
    );
  }
}

export default LogInForm;
