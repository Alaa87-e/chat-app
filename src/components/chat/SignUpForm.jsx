import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/Form";
import AuthService from "../../Services/authService";

class SignUpForm extends Form {
  state = {
    data: {
      userName: "",
      email: "",
      password: "",
    },
    errors: {},
  };
  schema = {
    userName: Joi.string().required().label("User Name"),
    password: Joi.string().required().label("Password"),
    email: Joi.string().email().required().label("Email"),
  };

  doSubmit = () => {
    let { errors, data } = { ...this.state };
    try {
      AuthService.signUp(data);
      window.location = "/";
    } catch (exception) {
      errors = errors || {};
      errors["submitingResuls"] = exception.message;
      this.setState({ errors, data });
    }
  };

  render() {
    return (
      <div>
        <h1>Sign up</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("userName", "User Name")}
          {this.renderInput("email", "Email", "email")}
          {this.renderInput("password", "Password", "password")}
          {this.renderValidationSummary()}
          {this.renderButton("Sign up")}
        </form>
      </div>
    );
  }
}

export default SignUpForm;
