import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/Form";
import ChatService from "../../Services/chatService";

class SendMessageForm extends Form {
  state = {
    data: {
      message: "",
    },
    errors: {},
  };
  schema = {
    message: Joi.string().allow(null, "").label("Message"),
  };

  doSubmit = () => {
    let { data } = { ...this.state };
    this.props.onSendMessageFormSubmist(data.message);
    this.setState({
      data: {
        message: "",
      },
      errors: {},
    });
  };

  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <form onSubmit={this.handleSubmit} className="row">
            <div className="col-12 col-sm pr-sm-0">
              {this.renderInput("message")}
            </div>
            <div className="col-12 col-sm-auto pl-sm-0">
              {this.renderButton("Send")}
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default SendMessageForm;
