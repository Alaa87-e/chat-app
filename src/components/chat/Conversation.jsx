import React, { Component } from "react";
import Table from "../common/Table";
class Conversation extends Component {
  render() {
    const { conversation, onDelete } = this.props;
    const columns = [
      {
        propertyPath: "recivedMessage",
        labelText: "",
        key: "recivedMessage",
        content: (conversationItem) => (
          <p>
            <span className="font-weight-bold">
              {conversationItem.recivedMessage
                ? conversationItem.from + ": "
                : ""}
            </span>
            <span className="badge badge-secondary">
              {conversationItem.recivedMessage}
            </span>
          </p>
        ),
      },
      {
        propertyPath: "date",
        labelText: "",
        key: "date",
        content: (conversationItem) => {
          let miliSeconds = parseInt(conversationItem.date);
          let date = new Date(miliSeconds);
          return (
            <p>
              <small className="text-muted">{date.toUTCString()}</small>
            </p>
          );
        },
      },
      {
        propertyPath: "sentMessage",
        labelText: "",
        key: "sentMessage",
        content: (conversationItem) => (
          <p>
            <span className="font-weight-bold">
              {conversationItem.sentMessage ? conversationItem.from + ": " : ""}
            </span>
            <span className="badge badge-primary">
              {conversationItem.sentMessage}
            </span>
          </p>
        ),
      },
      {
        key: "delete",
        content: (conversationItem) => (
          <button
            className="btn btn-danger btn-xs m-2"
            onClick={() => onDelete(conversationItem.date)}
          >
            <i className="fa fa-trash-o"></i>
          </button>
        ),
      },
    ];

    return (
      <Table
        columns={columns}
        rows={conversation}
        rowIdPropertyPath="date"
        sortColumn={""}
        onSort={() => {}}
      />
    );
  }
}
export default Conversation;
