import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
const NavBar = ({ currentUser }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <Link className="navbar-brand" to="/">
        <img
          src={require("../../imgs/chat.png")}
          width="30"
          height="30"
          alt=""
        />
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink className="nav-item nav-link " to="/messenger">
            Messenger
          </NavLink>
          {!currentUser && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/login">
                Log in
              </NavLink>
              <NavLink className="nav-item nav-link" to="/signup">
                Sign up
              </NavLink>
            </React.Fragment>
          )}
          {currentUser && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/">
                {currentUser.userName}
              </NavLink>
              <NavLink className="nav-item nav-link" to="/logout">
                Log out
              </NavLink>
            </React.Fragment>
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
