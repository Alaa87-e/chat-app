import React from "react";
const NotAuthenticated = () => {
  return (
    <React.Fragment>
      <div className="row">
        <h1>Welcome to our chat app</h1>
      </div>
      <div className="row">
        <p>You are not logged in please log in or sign up </p>
      </div>
    </React.Fragment>
  );
};

export default NotAuthenticated;
