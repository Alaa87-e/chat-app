import React, { Component } from "react";
const ContactsList = (props) => {
  const { contacts, selectedContact, onSelectedContactChange } = props;
  return (
    <ol className="list-group list-group-numbered">
      {contacts.map((contact) => (
        <li
          key={contact.name}
          className={
            selectedContact && selectedContact.name === contact.name
              ? "list-group-item d-flex justify-content-between align-items-start active clickable"
              : "list-group-item d-flex justify-content-between align-items-start clickable"
          }
          onClick={() => onSelectedContactChange(contact)}
        >
          <div className="ms-2 me-auto">
            <div className="font-weight-bold">{contact.name}</div>
            {contact.lastMessage}
          </div>
          {contact.unreadMessagesCount ? (
            <span className="badge bg-danger rounded-pill">
              {contact.unreadMessagesCount}
            </span>
          ) : (
            <span className="badge bg-secondary rounded-pill">
              {contact.unreadMessagesCount}
            </span>
          )}
        </li>
      ))}
    </ol>
  );
};

export default ContactsList;
