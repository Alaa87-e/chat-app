import React from "react";

import TableHeader from "./../common/TableHeader";
import TableBody from "../common/TableBody";
const Table = ({ columns, sortColumn, onSort, rows, rowIdPropertyPath }) => {
  return (
    <table className="table">
      <TableHeader columns={columns} sortColumn={sortColumn} onSort={onSort} />
      <TableBody
        rows={rows}
        columns={columns}
        rowIdPropertyPath={rowIdPropertyPath}
      />
    </table>
  );
};

export default Table;
