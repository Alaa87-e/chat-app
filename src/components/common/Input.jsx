import React, { Component } from "react";
const Input = ({ name, label, error, ...rest }) => {
  return (
    <div className="form-group">
      {label ? <label htmlFor={name}>{label}</label> : null}
      <input id={name} name={name} {...rest} className="form-control" />
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Input;
