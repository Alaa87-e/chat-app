import React, { Component } from "react";

class TableHeader extends Component {
  raiseSort = (propertyPath) => {
    let sortColumn = { ...this.props.sortColumn };
    sortColumn.propertyPath === propertyPath && sortColumn.sortType === "asc"
      ? (sortColumn.sortType = "desc")
      : (sortColumn = { propertyPath, sortType: "asc" });
    this.props.onSort(sortColumn);
  };
  renderSortIcon = (column) => {
    if (column.propertyPath !== this.props.sortColumn.propertyPath) {
      return null;
    } else if (this.props.sortColumn.sortType === "asc") {
      return <i className="fa fa-sort-asc"></i>;
    } else {
      console.log(this.props.sortColumn.sortType);
      return <i className="fa fa-sort-desc"></i>;
    }
  };
  render() {
    const { columns } = this.props;
    return (
      <thead>
        <tr>
          {columns.map((column) => (
            <th
              className="clickable"
              key={column.propertyPath || column.key}
              onClick={() => this.raiseSort(column.propertyPath)}
            >
              {column.labelText}
              {this.renderSortIcon(column)}
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
