import React, { Component } from "react";
import Joi from "joi-browser";
import Input from "./Input";
class Form extends Component {
  state = {
    data: {},
    errors: {},
  };
  validateInput = ({ name, value }) => {
    const obj = { [name]: value };
    const objSchema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, objSchema);
    return error ? error.details[0].message : null;
  };
  validate = () => {
    const options = {
      abortEarly: false,
    };
    const { error } = Joi.validate(this.state.data, this.schema, options);
    const errors = {};
    if (!error) return null;
    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }
    return errors;
  };
  handleSubmit = (e) => {
    e.preventDefault();
    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;
    this.doSubmit();
  };
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    if (errors["submitingResuls"]) {
      delete errors["submitingResuls"];
    }
    const errorMessage = this.validateInput(input);
    if (errorMessage) {
      errors[input.name] = errorMessage;
    } else {
      delete errors[input.name];
    }
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data, errors: errors || {} });
  };
  renderButton = (label) => {
    return (
      <button disabled={this.validate()} className="btn btn-primary btn-block">
        {label}
      </button>
    );
  };
  renderInput = (name, label, type = "text") => {
    const { data, errors } = this.state;
    return (
      <Input
        name={name}
        label={label}
        value={data[name]}
        type={type}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };
  renderValidationSummary = () => {
    const { errors } = this.state;
    let errorsAsHtmlLinesInAList = [];
    for (let error in errors) {
      errorsAsHtmlLinesInAList.push(
        <li className="text-muted" key={error}>
          <small>{errors[error]}</small>
        </li>
      );
    }
    return errorsAsHtmlLinesInAList.length ? (
      <div className="alert alert-danger">
        <ul>{errorsAsHtmlLinesInAList}</ul>
      </div>
    ) : (
      <div></div>
    );
  };
}

export default Form;
