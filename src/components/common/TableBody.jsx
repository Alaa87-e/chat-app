import React from "react";
import _ from "lodash";
const TableBody = ({ rows, rowIdPropertyPath, columns }) => {
  return (
    <tbody>
      {rows.map((row) => (
        <tr key={_.get(row, rowIdPropertyPath)}>
          {columns.map((column) =>
            column.key ? (
              <td key={_.get(row, rowIdPropertyPath) + column.key}>
                {column.content(row)}
              </td>
            ) : (
              <td key={_.get(row, rowIdPropertyPath) + column.propertyPath}>
                {_.get(row, column.propertyPath)}
              </td>
            )
          )}
        </tr>
      ))}
    </tbody>
  );
};

export default TableBody;
