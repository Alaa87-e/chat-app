/**
 * This is a fuction that we used to hash strings
 * like passowrds
 * @param stringToHash is the string that we need to hash
 */

export function hashString(stringToHash) {
  var hash = 0,
    i,
    chr;
  if (stringToHash.length === 0) return hash;
  for (i = 0; i < stringToHash.length; i++) {
    chr = stringToHash.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
}
