import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./components/chat/NavBar";
import Messenger from "./components/chat/Messenger";
import NotAuthenticated from "./components/chat/NotAuthenticated";
import NotFound from "./components/chat/NotFound";
import LogOut from "./components/chat/LogOut";
import LogInForm from "./components/chat/LogInForm";
import SignUpForm from "./components/chat/SignUpForm";
import AuthService from "./Services/authService";
import { ToastContainer } from "react-toastify";
import _ from "lodash";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Footer from "./components/chat/Footer";

class App extends Component {
  state = {
    currentUser: {},
  };
  componentDidMount() {
    const currentUser = AuthService.getCurrentLoggedInUser();
    this.setState({ currentUser });
  }
  render() {
    return (
      <React.Fragment>
        <ToastContainer />
        <NavBar currentUser={this.state.currentUser} />
        <main className="container pt-3">
          <Switch>
            <Route path="/login" component={LogInForm}></Route>
            <Route path="/logout" component={LogOut}></Route>
            <Route path="/signup" component={SignUpForm}></Route>
            <Route
              path="/messenger"
              render={() =>
                !_.isEmpty(this.state.currentUser) ? (
                  <Messenger />
                ) : (
                  <NotAuthenticated />
                )
              }
            ></Route>
            <Redirect from="/" exact to="/messenger"></Redirect>
            <Route path="/" component={NotFound}></Route>
          </Switch>
        </main>
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
