## **Chat-App Introduction**

our **Chat-App** is a frontend of a simple messenger that enables the users to make text conversations. This app has been implemented using ReactJS and Bootstrap. 

In this app, we have not implemented the app backend, but we emulated the backend by storing all chat data locally  using  Local Storage. 

To implement our app frontend we used some JS libraries, also we depended on some generic components which have been implemented by us during some previous projects, like a generic table and a generic form. 

In our implementation of the app, we tried to let the code compatible with javascript code style, and to do that we used a VS Code plugin called prettier. Also in our code we tried to be compatible with ReactJS naming conventions.

The application has been deployed to a free host, and you can view it using the following [link](http://chat-app-alaa-abou-ahmad.rf.gd)

---

## Used Tools an Libraries

The code of this project has been created by using the following tools and libraries:

1. **create-react-app** version 1.5.2.
2.  **bootstrap** version 4.1.1.
3.  **react-toastify** version 4.1.
4.  **joi-browser** version 13.4.
5.  **lodash** version 4.17.10.
6.  **font-awesome** version 4.7.0.
6.  **react-router-dom** version 4.3.1.

---
if you want to setup the project locally kindly follow the following steps:

1. Open your terminal and then type $ git clone {the url to the GitHub repo} 
2. cd into the new folder and type $ npm install
3. To run the React project. $ npm start

---
## App React Components

To develop the app we have built more than 10 components and the most important components are:

1. **App**: this component is the parent component.
2. **Messenger**: this component is the most important component in the app because it implements the whole chat logic .
3. **ContactsList**, **Conversation**, and **SendMessageForm**. Those components represents the business logic and the parent of those three components is **Messenger** component .
4. **SignUpForm**, **LogInForm** and **LogOut**, which are the components that are responsible for managing the users authentication and authorization .
5. **NavBr** ,**Footer**, and some other not important components that are responsible for displaying some errors.

The most important point that we have to mention here, is that some of the components that are listed above depends on some generic components that have been implemented by us previously.

For example all the form components like SignUpForm, LogInForm, and SendMessageForm inherits from a base generic component called **Form**. 

Also our table component which called Conversation uses in its render method a generic component called **Table**.

---

## Project Files Structure

In this project we depended on the files and folders structure that has been created by create-react-app@1.5.2 , but we added some folders like the services folder that contains the services that are supposed to be responsible for calling the backend and preparing the data for the components. Another folder that we added is the util folder which contains the utilities of the components and services. Also we divided the components folder into two folders: the first folder is chat folder that contains our app components, and the  other folder is the common folder that contains the generic components.

## Data Storage

We stored all the data in the Local Storage in the browser. i.e. we  stored the conversations, in addition to the users and the currently logged in user data in the Local Storage.

## Proposed Enhancements and Future Work

- Currently the application can be considered as a responsive web app but we can make some minor css changes to make it more responsive.
- In the future, we need to store the currently logged in user data (credentials or token) in memory, not in the Local storage. That's to avoid any cross site scripting attack. And this thing is easy because the logic of the user management , authentication and authorization is currently localized in a single service file which is "authService.js", and to change this logic we need to reimplement the functions of this file.







